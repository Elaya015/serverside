const db = require("../entity");
const Company = db.company;

// Create and Save a new User
exports.create = (req, res) => {
    console.log("Hello raja");
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }


  // Create a user
  const CompanyInfo = {
    companyName: req.body.userId,
    RegistrationNo: req.body.RegistrationNo,
    RegisteredAddress: req.body.RegisteredAddress,
    City: req.body.City,
    State: req.body.State,
    PANNO: req.body.PANNO,
    EstablishmentYear: req.body.EstablishmentYear,
    CompanyCategory: req.body.CompanyCategory,
    BusinessNature: req.body.BusinessNature,
    ContactName: req.body.ContactName,
    Email: req.body.Email,
    Designation: req.body.Designation,
    Phone: req.body.Phone,
    companyId: req.body.companyId
};

     Company.create(CompanyInfo)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};


// Retrieve all User from the database.
exports.findAll = (req, res) => {
    const userId = req.query.userId;
  
    Company.findAll({ where: userId })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving User."
        });
      });
  };


  // Find a single User with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Company.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving User with id=" + id
        });
      });
  };


  // Update a Tutorial by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Company.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
  };


  // Delete a User with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Company.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "USER was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete user with id=${id}. Maybe User was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete User with id=" + id
        });
      });
  };



  // Delete all User from the database.
exports.deleteAll = (req, res) => {
    Company.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} User were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all user."
        });
      });
  };
  module.exports=exports;
  