const db = require("../entity");
const Tender = db.tender;

// Create and Save a new User
exports.create = (req, res) => {
    console.log("Hello raja");
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }
 // Create a tender
  const tenderInfo = {
    TenderRefNo: req.body.TenderRefNo,
    TenderType: req.body.TenderType,
    Form_Of_Contract: req.body.Form_Of_Contract,
    No_of_covers: req.body.No_of_covers,
    Account_Type_Head: req.body.Account_Type_Head,
    PaymentMode: req.body.PaymentMode,
    Document_Description: req.body.Document_Description,
    Document_Type: req.body.Document_Type,
    Document_Upoladed_Date: req.body.Document_Upoladed_Date,
    Product_Catagory: req.body.Product_Catagory,
    Product_SubCatagory: req.body.Product_SubCatagory,
    Location: req.body.Location,
    Tender_Value: req.body.Tender_Value,
    Delivery_Period: req.body.Delivery_Period,
    userId: req.body.userId,
};

     Tender.create(tenderInfo)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};


// Retrieve all User from the database.
exports.findAll = (req, res) => {
    const userId = req.query.userId;
  
    Tender.findAll({ where: userId })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving User."
        });
      });
  };


  // Find a single User with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Tender.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving User with id=" + id
        });
      });
  };


  // Update a Tutorial by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Tender.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
  };


  // Delete a User with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Tender.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "USER was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete user with id=${id}. Maybe User was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete User with id=" + id
        });
      });
  };



  // Delete all User from the database.
exports.deleteAll = (req, res) => {
    Tender.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} User were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all user."
        });
      });
  };
  module.exports=exports;
  