const db = require("../entity");
const User = db.user;
const Company=db.company;
const Tender=db.tender;
var APPPATH     = require('app-root-path');
const Entity      = require(APPPATH + '/app/entity');

// Create and Save a new User
exports.create = (req, res) => {
    console.log("Hello raja");
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a user
  const UserInfo = {
    userId: req.body.userId,
    userName: req.body.userName,
    Organisation: req.body.Organisation,
    LoginId: req.body.LoginId,
    mobileNumber: req.body.mobileNumber,
    Password: req.body.Password,
    companyId: req.body.companyId
};

     User.create(UserInfo)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};





// Retrieve all User from the database.
exports.findAll = (req, res) => {
  console.log("enter value");
  const userId = req.query.userId;

  Entity.company.findAll({ 
    where      : {'companyId':'12'},
    attributes : ['companyId'],
    include : [{
        model      : Entity.user,
        where      : {'userId': '1'},include:[{model:Entity.tender}]
    }],
})
.then((values) => { 
    if(!values) return null;
    console.log("values--->"+values);   
    res.send(values); 
    return values;
})
.catch((err) => {
  console.log("values--->"+err);    

    return new Error(err);
});

};


  // Find a single User with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  

    Entity.user.findOne({ 
      where      : {'userId':id},
      include : [{
          model      : Entity.tender
      }],
  })
  .then((values) => { 
      if(!values) return null;
      console.log("values--->"+values);   
      res.send(values); 
      return values;
  })
  .catch((err) => {
    console.log("values--->"+err);    
    return new Error(err);
  });
  };


  // Update a Tutorial by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    User.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
  };


  // Delete a User with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    User.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "USER was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete user with id=${id}. Maybe User was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete User with id=" + id
        });
      });
  };



  // Delete all User from the database.
exports.deleteAll = (req, res) => {
    User.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} User were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all user."
        });
      });
  };
  module.exports=exports;
  