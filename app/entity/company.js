const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define('company', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    companyName: {
        type: Sequelize.STRING
    },
    RegistrationNo: {
        type: Sequelize.INTEGER
    },
    RegisteredAddress: {
        type: Sequelize.TEXT
    },
    City: {
        type: Sequelize.STRING
    },
    State: {
        type: Sequelize.STRING
    },
    PostalCode: {
        type: Sequelize.STRING
    },
    PANNO: {
        type: Sequelize.INTEGER
    },
    EstablishmentYear: {
        type: Sequelize.STRING
    },
    CompanyCategory: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    ContactName: {
        type: Sequelize.STRING
    },
    DOB: {
        type: Sequelize.STRING
    },
    Email: {
        type: Sequelize.STRING
    },
    Designation: {
        type: Sequelize.STRING
    },
    Phone: {
        type: Sequelize.STRING
    },
   companyId: {
        type: Sequelize.STRING,
                primaryKey: true

    }
}, {freezeTableName: true});
Company.associate = models => {
    Company.hasMany(models.user,{foreignKey:'companyId', sourceKey: 'companyId', constraints: false });
    }
 return Company;
};
