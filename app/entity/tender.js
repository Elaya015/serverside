const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
 const Tender = sequelize.define('tender', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    TenderRefNo: {
        type: Sequelize.INTEGER
    },
    TenderType: {
        type: Sequelize.INTEGER
    },
    Form_Of_Contract: {
        type: Sequelize.TEXT
    },
    No_of_covers: {
        type: Sequelize.INTEGER
    },
    Account_Type_Head: {
        type: Sequelize.STRING
    },
    PaymentMode: {
        type: Sequelize.STRING
    },
    Document_Description: {
        type: Sequelize.STRING
    },
    Document_Type: {
        type: Sequelize.STRING
    },
    Document_Upoladed_Date: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    Product_Catagory: {
        type: Sequelize.STRING
    },
    Product_SubCatagory: {
        type: Sequelize.STRING
    },
    Location: {
        type: Sequelize.STRING
    },
    Tender_Value: {
        type: Sequelize.STRING
    },
    Delivery_Period: {
        type: Sequelize.STRING
    },
    userId: {
        type: Sequelize.INTEGER
    },
}, {freezeTableName: true});
return Tender;
};