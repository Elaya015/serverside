'use strict';


module.exports = (sequelize, Sequelize) => {
const User= sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userId: {
        type: Sequelize.INTEGER,
    },
    userName: {
        type: Sequelize.STRING,
    },
    Organisation: {
        type: Sequelize.STRING,
    },
    LoginId: {
        type: Sequelize.STRING,
    },
    mobileNumber: {
        type: Sequelize.STRING,
        field: 'mobile_number', defaultValue: 0
    } ,
    Password: {
        type: Sequelize.STRING,
    }
    ,companyId: {
        type: Sequelize.STRING,
    }
}, {freezeTableName: true});
User.associate = models => {
User.hasMany(models.tender,{foreignKey:'userId', sourceKey: 'userId', constraints: false });
}
User.sync({force: false});
return User;
};