    const company = require("../controller/companyController.js");
  
    var router = require("express").Router();
  
    router.post("/company", company.create);  
    router.get("/company", company.findAll);
    router.get("/company/:id", company.findOne);
    router.put("/company/:id", company.update);
    router.delete("/company/:id", company.delete);
    module.exports=router;