    const tender = require("../controller/tenderController.js");
  
    var router = require("express").Router();
  
    router.post("/tender", tender.create);
  
    router.get("/tender", tender.findAll);
  
    router.get("/tender/:id", tender.findOne);
  
    router.put("/tender/:id", tender.update);
  
    router.delete("/tender/:id", tender.delete);
    module.exports=router;