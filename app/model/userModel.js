    const user = require("../controller/userController");
  
    var router = require("express").Router();
  
    router.post("/user", user.create);
    router.get("/user", user.findAll);
    router.get("/user/:id", user.findOne);
    router.put("/user/:id", user.update);
    router.delete("/user/:id", user.delete);
    router.delete("/user", user.deleteAll);
    module.exports=router;