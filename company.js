const express = require('express');
const pg = require('pg');
const app = express();
const port = 3005;
const cors = require('cors')
app.use(cors());
app.get('/', (req, res) => res.send('User List App'));

const Sequelize = require('sequelize');

  const sequelize = new Sequelize('etender', 'etender', 'Etender!2#', {
   host: "35.232.140.124",
    port:5432,
    dialect: 'postgres',
    logging: false
  });

//const sequelize = new Sequelize('postgres://etender:Etender!2#@35.232.140.124:5432/etender');

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  const Company2 = sequelize.define('company', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    companyName: {
        type: Sequelize.STRING
    },
    RegistrationNo: {
        type: Sequelize.INTEGER
    },
    RegisteredAddress: {
        type: Sequelize.TEXT
    },
    City: {
        type: Sequelize.STRING
    },
    State: {
        type: Sequelize.STRING
    },
    PostalCode: {
        type: Sequelize.STRING
    },
    PANNO: {
        type: Sequelize.INTEGER
    },
    EstablishmentYear: {
        type: Sequelize.STRING
    },
    CompanyCategory: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    ContactName: {
        type: Sequelize.STRING
    },
    DOB: {
        type: Sequelize.STRING
    },
    Email: {
        type: Sequelize.STRING
    },
    Designation: {
        type: Sequelize.STRING
    },
    Phone: {
        type: Sequelize.STRING
    },
   
    CompanyID: {
        type: Sequelize.STRING
    }
}, {freezeTableName: true});


  // Table created
 Company2.bulkCreate([
      {id:'1', companyName: 'RK Traders', RegistrationNo: '12345',RegisteredAddress:'chennai',City:'chennai',State:'TN',PostalCode:'636119',PANNO:'908745',EstablishmentYear:'2013',CompanyCategory:'PEST control',BusinessNature:'MSME',ContactName:'Ravi',DOB:'1980/05/01',Email:'rk@gmail.com',Designation:'Deputy Manager',Phone:'8148582763',CompanyID:'12' },
       {id:'2', companyName: 'Wipro', RegistrationNo: '324125',RegisteredAddress:'Bangalore',City:'Bangalore',State:'KA',PostalCode:'530233',PANNO:'234563',EstablishmentYear:'2009',CompanyCategory:'XXZY',BusinessNature:'QWSA',ContactName:'RAJA',DOB:'1989/03/21',Email:'wp@gmail.com',Designation:'1234',Phone:'8148582762',CompanyID:'14' }
    ]).then(function() {
      return Company2.findAll();
    }).then(function(company) {
      console.log(company);
    });




 
app.listen(port, () => console.log(`notes-app listening on port ${port}!`));