const express = require('express');
const pg = require('pg');
const app = express();
const port = 3005;
const cors = require('cors')
app.use(cors());
app.get('/', (req, res) => res.send('User List App'));

const Sequelize = require('sequelize');

  const sequelize = new Sequelize('etender', 'etender', 'Etender!2#', {
   host: "35.232.140.124",
    port:5432,
    dialect: 'postgres',
    logging: false
  });


sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  const Company = sequelize.define('company', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true
    },
    companyName: {
        type: Sequelize.STRING
    },
    RegistrationNo: {
        type: Sequelize.INTEGER
    },
    RegisteredAddress: {
        type: Sequelize.TEXT
    },
    City: {
        type: Sequelize.STRING
    },
    State: {
        type: Sequelize.STRING
    },
    PostalCode: {
        type: Sequelize.STRING
    },
    PANNO: {
        type: Sequelize.INTEGER
    },
    EstablishmentYear: {
        type: Sequelize.STRING
    },
    CompanyCategory: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    ContactName: {
        type: Sequelize.STRING
    },
    DOB: {
        type: Sequelize.STRING
    },
    Email: {
        type: Sequelize.STRING
    },
    Designation: {
        type: Sequelize.STRING
    },
    Phone: {
        type: Sequelize.STRING
    },
   companyId: {
        type: Sequelize.STRING,
                primaryKey: true

    }
}, {freezeTableName: true});


Company.sync().then(() => {
  // Table created
  return 
 Company.bulkCreate([
      { companyName: 'RK Traders', RegistrationNo: '12345',RegisteredAddress:'chennai',City:'chennai',State:'TN',PostalCode:'636119',PANNO:'908745',EstablishmentYear:'2013',CompanyCategory:'PEST control',BusinessNature:'MSME',ContactName:'Ravi',DOB:'1980/05/01',Email:'rk@gmail.com',Designation:'Deputy Manager',Phone:'8148582763',companyId:'12' },
       { companyName: 'Wipro', RegistrationNo: '324125',RegisteredAddress:'Bangalore',City:'Bangalore',State:'KA',PostalCode:'530233',PANNO:'234563',EstablishmentYear:'2009',CompanyCategory:'XXZY',BusinessNature:'QWSA',ContactName:'RAJA',DOB:'1989/03/21',Email:'wp@gmail.com',Designation:'1234',Phone:'8148582762',companyId:'14' }
    ]).then(function() {
      return Company.findAll();
    }).then(function(company) {
      console.log(company);
    });

});


const Tender = sequelize.define('tender', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    TenderRefNo: {
        type: Sequelize.INTEGER
    },
    TenderType: {
        type: Sequelize.INTEGER
    },
    Form_Of_Contract: {
        type: Sequelize.TEXT
    },
    No_of_covers: {
        type: Sequelize.INTEGER
    },
    Account_Type_Head: {
        type: Sequelize.STRING
    },
    PaymentMode: {
        type: Sequelize.STRING
    },
    Document_Description: {
        type: Sequelize.STRING
    },
    Document_Type: {
        type: Sequelize.STRING
    },
    Document_Upoladed_Date: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    Product_Catagory: {
        type: Sequelize.STRING
    },
    Product_SubCatagory: {
        type: Sequelize.STRING
    },
    Location: {
        type: Sequelize.STRING
    },
    Tender_Value: {
        type: Sequelize.STRING
    },
    Delivery_Period: {
        type: Sequelize.STRING
    },
    userId: {
        type: Sequelize.INTEGER
    },
}, {freezeTableName: true});


  // Table created
 Tender.bulkCreate([
      { TenderRefNo: '20201234',TenderType:'1',Form_Of_Contract:'supply',No_of_covers:'2',Account_Type_Head:'Goods',Account_Type_Head:'State Government funded',PaymentMode:'offline',Document_Description:'Scanned copy of tender fee',Document_Type:'pdf',Document_Upoladed_Date:'2020/10/05 10:10:10',Product_Catagory:'computer H/w',Product_SubCatagory:'RAM',Location:'NewDelhi',Tender_Value:'0',Delivery_Period:'120',userId:'001' },
          { TenderRefNo: '20201235',TenderType:'1',Form_Of_Contract:'supply',No_of_covers:'2',Account_Type_Head:'Goods',Account_Type_Head:'State Government funded',PaymentMode:'offline',Document_Description:'Scanned copy of tender fee',Document_Type:'pdf',Document_Upoladed_Date:'2020/10/05 10:10:10',Product_Catagory:'computer H/w',Product_SubCatagory:'RAM',Location:'NewDelhi',Tender_Value:'0',Delivery_Period:'120',userId:'002' }
    ]).then(function() {
      return Tender.findAll();
    }).then(function(tender) {
      console.log(tender);
    });


const User= sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userId: {
        type: Sequelize.INTEGER,
    },
    userName: {
        type: Sequelize.STRING,
    },
    Organisation: {
        type: Sequelize.STRING,
    },
    LoginId: {
        type: Sequelize.STRING,
    },
    mobileNumber: {
        type: Sequelize.STRING,
        field: 'mobile_number', defaultValue: 0
    } ,
    Password: {
        type: Sequelize.STRING,
    }
    ,companyId: {
        type: Sequelize.STRING,
    }
}, {freezeTableName: true});
  

User.sync().then(() => {
  // Table created
 User.bulkCreate([
      { userId: '001', userName:'Deptone',Organisation:'NIC',LoginId:'ecdep1@yahoo.co.in',mobileNumber:'8867664180',Password:'ecdep@123',companyId:'12' },
      { userId: '002', userName:'Depttwo',Organisation:'PSA',LoginId:'ecdepttwo@yahoo.co.in',mobileNumber:'8867664181',Password:'ecedepttwo@123',companyId:'12' },
       { userId: '003', userName:'Deptthree',Organisation:'NIC',LoginId:'ecdeptthree@yahoo.co.in',mobileNumber:'8867664180',Password:'ecdeptthree@123',companyId:'14' }
    ]).then(function() {
      return User.findAll();
    }).then(function(user) {
      console.log(user);
    });

});


User.hasMany(Tender,{foreignKey:'userId'});
Company.hasMany(User,{foreignKey:'companyId'});

sequelize.sync()
.then(() => {
  console.log(`Database & tables created!`);

app.get('/getAllTenders', function(req, res) {
    Company.findAll({
     attributes:['companyId'],
        include: [{
        model: User, attributes:['userId'], include: [{model:Tender}]
  }]
  });
  });

  app.get('/company/getAllUsers', function(req, res) {
    Company.findAll({
     include: [{
    model: User
  }]
});
  });

 app.get('/tender/delete', function(req, res) {
    Tender.destroy({
     where: {},
     truncate: true
})
  });
 });



User.findAll({
     attributes:['userId'],
  include: [{
    model: Tender,
  
  }]
  });

 
app.listen(port, () => console.log(`notes-app listening on port ${port}!`));