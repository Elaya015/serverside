const express = require('express');
const app = express();
const port = 3000;
const cors = require("cors");
const bodyParser = require("body-parser");

app.get('/', (req, res) => res.send('Notes App'));

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const db = require("./app/config/dbconfig");


app.use("/api", require('./app/model/userModel'));

//app.use("/api/company", require('./app/model/companyModel'));
//app.use("/api/user", require('./app/model/userModel'));
//app.use("/api/tender",require('./app/model/tenderModel'));



//require("./app/routes/etender.route")(app);
//require("./app/routes/etender.route")(app);
//var routes=require("./app/model/userModel")

// set port, listen for requests
const PORT = 3002;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
