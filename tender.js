const express = require('express');
const pg = require('pg');
const app = express();
const port = 3005;
const cors = require('cors')
app.use(cors());
app.get('/', (req, res) => res.send('User List App'));

const Sequelize = require('sequelize');

  const sequelize = new Sequelize('etender', 'etender', 'Etender!2#', {
   host: "35.232.140.124",
    port:5432,
    dialect: 'postgres',
    logging: false
  });

//const sequelize = new Sequelize('postgres://etender:Etender!2#@35.232.140.124:5432/etender');

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  
const Tender = sequelize.define('tender',{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    TenderRefNo: {
        type: Sequelize.INTEGER
    },
    TenderType: {
        type: Sequelize.INTEGER
    },
    Form_Of_Contract: {
        type: Sequelize.TEXT
    },
    No_of_covers: {
        type: Sequelize.INTEGER
    },
    Account_Type_Head: {
        type: Sequelize.STRING
    },
    PaymentMode: {
        type: Sequelize.STRING
    },
    Document_Description: {
        type: Sequelize.STRING
    },
    Document_Type: {
        type: Sequelize.STRING
    },
    Document_Upoladed_Date: {
        type: Sequelize.STRING
    },
    BusinessNature: {
        type: Sequelize.STRING
    },
    Product_Catagory: {
        type: Sequelize.STRING
    },
    Product_SubCatagory: {
        type: Sequelize.STRING
    },
    Location: {
        type: Sequelize.STRING
    },
    Tender_Value: {
        type: Sequelize.STRING
    },
    Delivery_Period: {
        type: Sequelize.STRING
    },
    UserID: {
        type: Sequelize.INTEGER
    },
}, {freezeTableName: true});


  // Table created
 Tender.bulkCreate([
      { TenderRefNo: '20201234',TenderType:'1',Form_Of_Contract:'supply',No_of_covers:'2',Account_Type_Head:'Goods',Account_Type_Head:'State Government funded',PaymentMode:'offline',Document_Description:'Scanned copy of tender fee',Document_Type:'pdf',Document_Upoladed_Date:'2020/10/05 10:10:10',Product_Catagory:'computer H/w',Product_SubCatagory:'RAM',Location:'NewDelhi',Tender_Value:'0',Delivery_Period:'120',UserID:'001' },
          { TenderRefNo: '20201234',TenderType:'1',Form_Of_Contract:'supply',No_of_covers:'2',Account_Type_Head:'Goods',Account_Type_Head:'State Government funded',PaymentMode:'offline',Document_Description:'Scanned copy of tender fee',Document_Type:'pdf',Document_Upoladed_Date:'2020/10/05 10:10:10',Product_Catagory:'computer H/w',Product_SubCatagory:'RAM',Location:'NewDelhi',Tender_Value:'0',Delivery_Period:'120',UserID:'002' }
    ]).then(function() {
      return Tender.findAll();
    }).then(function(tender) {
      console.log(tender);
    });





sequelize.sync()
.then(() => {
  console.log(`Database & tables created!`);


 });
 
app.listen(port, () => console.log(`notes-app listening on port ${port}!`));