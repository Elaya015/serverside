const express = require('express');
const pg = require('pg');
const app = express();
const port = 3005;
const cors = require('cors')
app.use(cors());
app.get('/', (req, res) => res.send('User List App'));

const Sequelize = require('sequelize');

  const sequelize = new Sequelize('etender', 'etender', 'Etender!2#', {
   host: "35.232.140.124",
    port:5432,
    dialect: 'postgres',
    logging: false
  });

//const sequelize = new Sequelize('postgres://etender:Etender!2#@35.232.140.124:5432/etender');

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


const User= sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userId: {
        type: Sequelize.INTEGER,
    },
    userName: {
        type: Sequelize.STRING,
    },
    Organisation: {
        type: Sequelize.STRING,
    },
    LoginId: {
        type: Sequelize.STRING,
    },
    mobileNumber: {
        type: Sequelize.STRING,
        field: 'mobile_number', defaultValue: 0
    } ,
    Password: {
        type: Sequelize.STRING,
    }
    , CompanyID: {
        type: Sequelize.STRING,
    }
}, {freezeTableName: true});

  // Table created
 User.bulkCreate([
      { userId: '001', userName: 'Deptone',Organisation:'NIC',LoginId:'ecdep1@yahoo.co.in',mobileNumber:'8867664180',Password:'ecdep@123',CompanyID:'12' },
      { userId: '002', userName: 'Depttwo',Organisation:'PSA',LoginId:'ecdepttwo@yahoo.co.in',mobileNumber:'8867664181',Password:'ecedepttwo@123',CompanyID:'12' },
       { userId: '003', userName: 'Deptthree',Organisation:'NIC',LoginId:'ecdeptthree@yahoo.co.in',mobileNumber:'8867664180',Password:'ecdeptthree@123',CompanyID:'14' }
    ]).then(function() {
      return User.findAll();
    }).then(function(user) {
      console.log(user);
    });



//Company.hasMany(User,{targetKey:'CompanyID',foreignkey:'CompanyID'})
//User.hasMany(Tender2,{targetKey:'userId',foreignKey:'userId'})



sequelize.sync({ force: false })
.then(() => {
  console.log(`Database & tables created!`);


  });
 
app.listen(port, () => console.log(`notes-app listening on port ${port}!`));